# README #

Este es el repositorio para el proyecto TFG de PhotoVoltaics. Esta es la parte encargada de realizar todo el tratamiento, procesado, Data Mining para dicho proyecto.

## Descripci�n ##
El contenido est� constituido por:
 
* Documentaci�n: aqu� ir� toda la documentaci�n, desarrollada en Latex
* Plantilla: plantilla proporcionada por Pedro Pernias
* Source Code: Aqu� ir� todo lo relacionado con codificaci�n y desarrollo. [R, Java, Scala, Spark]

## NOTAS importantes: ##

Este TFG se realiza mediante la colaboraci�n de multiples partes, como:

* Una primera parte encargada de realizar la recopilaci�n de datos, realizada por Antonio Mart�nez Galva� https://bitbucket.org/tfg_pv/tfg_antonio
* La parte del cliente, realizada por Melody