package scrapingtool.mappers;

import scrapingtool.entities.History;
import scrapingtool.entities.HistoryClient;

import java.time.ZoneId;

public class HistoryMapper {
    public static HistoryClient mapToClient(History history) {
        final HistoryClient historyClient = new HistoryClient();
        historyClient.setTime(history.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        historyClient.setEnergy(history.getEnergy().orElse(null));
        historyClient.setEfficiency(history.getEfficiency().orElse(null));
        historyClient.setPower(history.getPower().orElse(null));
        historyClient.setAverage(history.getAverage().orElse(null));
        historyClient.setNormalised(history.getNormalised().orElse(null));
        historyClient.setTemperature(history.getTemperature().orElse(null));
        historyClient.setEnergyUsed(history.getEnergyUsed().orElse(null));
        historyClient.setPowerUsed(history.getPowerUsed().orElse(null));

        return historyClient;
    }
}
