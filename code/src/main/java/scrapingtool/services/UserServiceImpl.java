package scrapingtool.services;

import org.eclipse.collections.impl.block.factory.HashingStrategies;
import org.eclipse.collections.impl.utility.ListIterate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scrapingtool.entities.HistoryClient;
import scrapingtool.entities.User;
import scrapingtool.repositories.UserRepository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by cbougeno on 10/1/18.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> obtainAllUsers() {
        return this.userRepository.findAll();
    }

    @Override
    public List<User> getUserByName(String name) {
        return this.userRepository.findByName(name);
    }

    @Override
    public User getUserByIdSid(String idSid) {
        return this.userRepository.findFirstByIdSid(idSid);
    }

    @Override
    public User addUser(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User saveUser(User user) {
        user.setId(null);
        System.out.println(user.toString());
        return this.userRepository.save(user);
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> ke) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(ke.apply(t), Boolean.TRUE) == null;
    }

    @Override
    public User updateUser(User thatUser, User thisUser) {
        if (null == thatUser) {
            return this.userRepository.save(thisUser);
        }
        System.out.println("Antes: " + thatUser.getHistories().size());

        final List<HistoryClient> mixHistories = new ArrayList<>();
        mixHistories.addAll(thatUser.getHistories());
        mixHistories.addAll(thisUser.getHistories());
        System.out.println("mix => " + mixHistories.size());

        ListIterate.distinct(mixHistories, HashingStrategies.fromFunction(HistoryClient::getTime));

        final Set<HistoryClient> newHistories = mixHistories.stream()
                .sorted((Comparator.comparing(HistoryClient::getTime)))
                .filter(distinctByKey(HistoryClient::getTime))
                .collect(Collectors.toSet());
        System.out.println(newHistories.toString());
        System.out.println("nuevas => " + newHistories.size());

        final Set<HistoryClient> allHistories = new HashSet<>(thatUser.getHistories());
        System.out.println("las de that: " + allHistories.size());

        allHistories.addAll(newHistories);
        System.out.println("tras añadir las nuevas: " + allHistories.size());

        final LinkedHashSet<HistoryClient> allOrderedHistories= allHistories.stream()
                .sorted((Comparator.comparing(HistoryClient::getTime)))
                .collect(Collectors.toCollection(LinkedHashSet::new));

        thatUser.setHistories(allOrderedHistories);
        thatUser.setName(thisUser.getName());

        System.out.println("Después: " + thatUser.getHistories().size());
        return this.userRepository.save(thatUser);
    }

}
