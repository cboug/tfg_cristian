package scrapingtool.services;

import scrapingtool.entities.User;

import java.util.List;


/**
 * Created by cbougeno on 10/1/18.
 */
public interface UserService {

    List<User> obtainAllUsers();

    List<User> getUserByName(String name);

    User getUserByIdSid(String idSid);

    User addUser(User user);

    User saveUser(User user);

    User updateUser(User thatUser, User thisUser);

}
