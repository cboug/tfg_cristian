package scrapingtool;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by cbougeno on 10/1/18.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName("scrapingtool")
                .apiInfo(apiInfo())
                .select()
                .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .paths((PathSelectors.any()))
                .build();
    }

    private ApiInfo apiInfo() {
        String description = "En este frontal se pueden encontrar todos los EndPoint y las entidades que serán necesarias para realizar las llamadas a la API</br>" +
                "Se pueden utilizar para realizar llamadas o testeo de la aplicación. El resultado de la llamada será almacenado en la base de datos de MongoDB</br>" +
                "Se tienen <b>cinco operaciones</b> en total:</br>" +
                "- Tres métodos para realizar llamadas contra el servicio de scraping.</br>" +
                "- Dos llamadas contra el servicio de MongoDB.";
        return new ApiInfoBuilder()
                .title("Scraping Tool TFG")
                .description(description)
                .version("1.0")
                .build();
    }
}
