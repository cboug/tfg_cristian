package scrapingtool.controllers;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import scrapingtool.entities.History;
import scrapingtool.entities.HistoryClient;
import scrapingtool.entities.User;
import scrapingtool.mappers.HistoryMapper;
import scrapingtool.services.UserService;
import scrapingtool.utils.Utilities;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by cbougeno on 5/11/17.
 */
@RestController
public class ScrapController {
    private Map<String, String>  cookies;
    private final String USER_AGENT = "Mozilla/5.0";

    @Autowired
    private UserService userService;


    @RequestMapping(method= RequestMethod.GET, value="/")
    public void scrapingWebToday(final String id, final String sid) throws Exception {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Utilities.Pattern.DATE_FORMAT);
        final String date = LocalDate.now().format(formatter);
        scrapingPvOutput(id, sid, date);
    }

    @RequestMapping(method= RequestMethod.GET, value="/date")
    public void scrapingPvOutput(final String id, final String sid, final String date) throws Exception {
        System.out.println("Start scraping PvOutput from " + date);
        final String idSid = id + sid;

        //Método encargado de obtener las cookies tras el login. Necesario para acceder a cada panel.
        loginToGetCookies(Utilities.Constant.URL_INDEX_PVOUTPUT);

        //Método que devuelve la URL montada a partir de los valores de entrada.
        final String url = getPvOutputIntradayURL(id, sid, date);

        //Se extrae el DOM de la web que se quiere hacer scraping.
        final Document dom = getDOM(url).orElseThrow(() -> new Exception("There's not found any DOM to extract panels info."));

        //Se llama a un método que extrae la información que se desea y la almacena en un listado de un objeto llamado HistoryClient.
        final Set<HistoryClient> histories = scrapToHistories(dom);

        //A continuación se extrae el nombre del panel del DOM.
        final Optional<String> name = getNameOfPanels(dom);

        //Con todos los elementos anteriores se crea una clase que contiene todos estos elementos.
        final User thisUser = new User();
        thisUser.setName(name.orElseThrow(() -> new Exception("There's not found name for the User of the panels.")));
        thisUser.setHistories(histories);
        thisUser.setIdSid(idSid);

        //A continuación, se comprueba si existe un usuario almacenado en BBDD y lo actualiza.
        final User thatUser = userService.getUserByIdSid(idSid);
        System.out.println(thisUser);

        userService.updateUser(thatUser, thisUser);

        System.out.println("End scraping PvOutput from " + date + " date");
    }

    @RequestMapping(method= RequestMethod.GET, value="/between")
    public void scrapingWebBetweenDates(final String id, final String sid, final String dtStart, final String dtEnd) throws Exception {
        System.out.println("Start scraping PvOutput from dates between " + dtStart + " and " + dtEnd);
        final String idSid = id + sid;

        //Método encargado de obtener las cookies tras el login. Necesario para acceder a cada panel.
        loginToGetCookies(Utilities.Constant.URL_INDEX_PVOUTPUT);

        /*Se obtiene los rangos de fechas de entrada, y por cada uno de ellos se monta la URL, para a continuación extraer
        el DOM y quedarse únicamente que aquellos que se han podido obtener.*/
        Supplier<Stream<Document>> streamDocs = () -> getDatesBetweenUsingJava8(dtStart, dtEnd).stream()
                .map(date -> getPvOutputIntradayURL(id, sid, date))
                .map(this::getDOM)
                .filter(Optional::isPresent)
                .map(Optional::get);

        //A continuación se extrae el nombre del panel del DOM.
        final Optional<String> name = getNameOfPanels(streamDocs.get().findFirst().orElseThrow(() -> new Exception("There's not found any extraction of PvOutput for find the name of the Panel.")));
        System.out.println(name);

        //Se realiza el scraping por cada uno de las fechas de entrada. A continuación se verticaliza y se lleva a un listado de HistoryClients.
        final Set<HistoryClient> panelSet = streamDocs.get()
                .map(this::scrapToHistories)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

        //Con todos los elementos anteriores se crea una clase que contiene todos estos elementos.
        final User thisUser = new User();
        thisUser.setName(name.orElseThrow(() -> new Exception("There's not found name for the User of the panels.")));
        thisUser.setIdSid(idSid);
        thisUser.setHistories(panelSet);

        //A continuación, se comprueba si existe un usuario almacenado en BBDD y lo actualiza.
        final User thatUser = userService.getUserByIdSid(idSid);

        userService.updateUser(thatUser, thisUser);

        System.out.println(thisUser);

        System.out.println("End scraping PvOutput from dates between " + dtStart + " and " + dtEnd);
    }

    private void loginToGetCookies(final String url) throws IOException {
        System.out.println("Start login on " + url + " ...");

        //Realiza la conexión POST con la URL de entrada para conectarse contra el alojamiento web.
        final Connection.Response loginPage = Jsoup.connect(url)
                .method(Connection.Method.POST)
                .data(Utilities.Dom.LOGIN, Utilities.Constant.USERNAME)
                .data(Utilities.Dom.PASSWORD, Utilities.Constant.PASSWORD)
                .followRedirects(true)
                .userAgent(USER_AGENT)
                .execute();

        //Tras el login se almacenan las cookies.
        System.out.println("Status Login: " + loginPage.statusCode() + "\nStart getting cookies ...");
        cookies = loginPage.cookies();
        System.out.println(loginPage.url());
        System.out.println("Cookies saved. End login ...");
    }

    private static List<String> getDatesBetweenUsingJava8(final String dtStart, final String dtEnd) {
        //Se establece el formato que se desea para las fechas
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Utilities.Pattern.DATE_FORMAT);
        LocalDate endDate = LocalDate.parse(dtEnd, formatter);
        LocalDate startDate = LocalDate.parse(dtStart, formatter);

        //Se obtienen los días entre la fecha origen y final.
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate) + 1;

        //Se construye un listado con todas las fechas entre la origen y final.
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(startDate::plusDays)
                .map(date -> date.format(formatter))
                .collect(Collectors.toList());
    }

    private String getPvOutputIntradayURL(final String id, final String sid, final String date) {
        //Se construye la URI para conectar a partir del servicio a extraer, los ids y la fecha.
        final String uri = "intraday.jsp?id=" + id + "&sid=" + sid + "&dt=" + date; //+ "&gs=0&m=0";
        cookies.put("URI", uri);
        return Utilities.Constant.URL_PVOUTPUT + uri;
    }

    private Optional<Document> getDOM(final String url) {
        System.out.println("Start getting url " + url + "  ...");
        try {
            //Se trata de conectar mediante una llamada GET contra la URL de entrada.
            final Connection.Response response = Jsoup.connect(url)
                    .method(Connection.Method.GET)
                    .followRedirects(true)
                    .cookies(cookies)
                    .userAgent(USER_AGENT)
                    .execute();
            System.out.println(response.statusCode());
            System.out.println(response.url());

            //Se hace una pausa de 3 segundos para que el sistema no interrumpa las llamadas al ser ejecutadas en cortos periodos de tiempo.
            Thread.sleep(3000);
            return Optional.of(response.parse());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private Set<HistoryClient> scrapToHistories(final Document doc) {
        //Se seleccionan todos los elementos TR del documento y se transforma a objeto Stream para trabajarlo a modo funcional.
        return doc.select("tr").stream()
                .map(Element::text) //Se pasa a string cada línea filtrada por TR.
                .filter(line -> line.matches(Utilities.Regex.BEGIN_NUMBER_ALL_PATTERN))//TODO better implementation for advoid headers?
                .map(line -> line.split(Utilities.Character.SPACE))
                .map(values -> {

                    String time = values[0] + "-" + values[1];
                    History history = new History();

                    //Se establece todos los valores extraidos en un objeto History
                    try {
                        if (values[1].matches("[AP]M$")) {
                            DateFormat formatTime = new SimpleDateFormat(Utilities.Regex.DATE_PATTERN);
                            history.setTime(formatTime.parse(time));
                        } else {
                            DateFormat formatTimeWithoutA = new SimpleDateFormat(Utilities.Regex.DATE_PATTERN_WITHOUT_A);
                            history.setTime(formatTimeWithoutA.parse(time));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    history.setEnergy(mapDecimalUnitStringToNumber(values[2]));
                    history.setEfficiency(mapDecimalUnitStringToNumber(values[3]));
                    history.setPower(mapDecimalUnitStringToNumber(values[4]));
                    history.setAverage(mapDecimalUnitStringToNumber(values[5]));
                    history.setNormalised(mapDecimalUnitStringToNumber(values[6]));
                    history.setTemperature(mapDecimalUnitStringToNumber(values[7]));
                    history.setVoltage(mapDecimalUnitStringToNumber(values[8]));
                    history.setEnergyUsed(mapDecimalUnitStringToNumber(values[9]));
                    history.setPowerUsed(mapDecimalUnitStringToNumber(values[10]));
                    return history;
                })
                .map(HistoryMapper::mapToClient)    //Se mappea al objeto que se pasará al repositorio.
                .sorted((Comparator.comparing(HistoryClient::getTime))) //Se ordena para escribirlo en repositorio.
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private Optional<String> getNameOfPanels(final Document dom) {
        // Devuelve el nombre extraido del DOM.
        return dom.select("tr").stream()
                .map(e -> Stream.of(e)
                        .map(Element::text)
                        .filter(s -> s.matches(Utilities.Regex.HEADER_PATTERN))
                        .map(s -> s.replaceAll(Utilities.Regex.HEADER_PATTERN, "$1"))
                        .findFirst().orElse("AlcachofaHardcodedø")
                ).findAny();
    }

    /** Utils for Casting data */
    @SuppressWarnings("unchecked")
    private <T> T getNumber(final String number) {
        if (number.contains(".")) {
            return (T) Float.valueOf(number);
        } else {
            return (T) Integer.valueOf(number);
        }
    }

    private <T extends Number> Optional<T>  mapDecimalUnitStringToNumber(final String number) {
        //Los nulos en la web se escriben con guión [-]. Si es así se devuelve un Optional EMPTY.
        if (number.equals("-")) {
            return Optional.empty();
        }

        //Se formatean los números por motivo de los puntos y comas.
        final String num = number
                .replaceAll(Utilities.Regex.DECIMAL_UNITS_PATTERN, Utilities.Regex.GROUP_1)
                .replace(Utilities.Character.COMMA, Utilities.Character.EMPTY);

        //Se obtiene el valor numérico, independientemente si es Integer o Float.
        return Optional.of(getNumber(num));
    }
}