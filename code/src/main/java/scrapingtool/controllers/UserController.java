package scrapingtool.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import scrapingtool.entities.User;
import scrapingtool.services.UserService;

import java.util.List;


/**
 * Created by cbougeno on 24/03/18.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(method= RequestMethod.GET, value="/{name}")
    public List<User> findUserByName(@PathVariable final String name) {
        return this.userService.getUserByName(name);
    }

    @RequestMapping(method= RequestMethod.GET, value="/search")
    public List<User> findAllUsers() {
        return this.userService.obtainAllUsers();
    }


}
