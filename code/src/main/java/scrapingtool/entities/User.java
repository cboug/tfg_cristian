package scrapingtool.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * Created by cbougeno on 10/1/18.
 */
@Document(collection = "user")
public class User {
    @Id
    private String id;
    private String name;
    private String idSid;
    private Integer numberPannels;
    private Integer powerPannel;
    private String pannelName;
    private String inverterName;
    private String location;
    private String orientation;
    private String tilt;
    private String timezone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy-HH:mm")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private Set<HistoryClient> histories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdSid() {
        return idSid;
    }

    public void setIdSid(String idSid) {
        this.idSid = idSid;
    }

    public Set<HistoryClient> getHistories() {
        return histories;
    }

    public void setHistories(Set<HistoryClient> histories) {
        this.histories = histories;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", idSid='" + idSid + '\'' +
                ", histories=" + histories +
                '}';
    }
}
