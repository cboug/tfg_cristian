package scrapingtool.entities;

import java.time.LocalDateTime;

/**
 * Created by cbougeno on 4/2/18.
 */
public class HistoryClient {

    private LocalDateTime time;
    private Float energy;
    private Float efficiency;
    private Integer power;
    private Integer average;
    private Float normalised;
    private Float temperature;
    private Float voltage;
    private Float energyUsed;
    private Integer powerUsed;
    private String sunligth;


    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Float getEnergy() {
        return energy;
    }

    public void setEnergy(Float energy) {
        this.energy = energy;
    }

    public Float getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Float efficiency) {
        this.efficiency = efficiency;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getAverage() {
        return average;
    }

    public void setAverage(Integer average) {
        this.average = average;
    }

    public Float getNormalised() {
        return normalised;
    }

    public void setNormalised(Float normalised) {
        this.normalised = normalised;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Float getVoltage() {
        return voltage;
    }

    public void setVoltage(Float voltage) {
        this.voltage = voltage;
    }

    public Float getEnergyUsed() {
        return energyUsed;
    }

    public void setEnergyUsed(Float energyUsed) {
        this.energyUsed = energyUsed;
    }

    public Integer getPowerUsed() {
        return powerUsed;
    }

    public void setPowerUsed(Integer powerUsed) {
        this.powerUsed = powerUsed;
    }

    @Override
    public String toString() {
        return "HistoryClient{" +
                "time=" + time +
                ", energy=" + energy +
                ", efficiency=" + efficiency +
                ", power=" + power +
                ", average=" + average +
                ", normalised=" + normalised +
                ", temperature=" + temperature +
                ", voltage=" + voltage +
                ", energyUsed=" + energyUsed +
                ", powerUsed=" + powerUsed +
                '}';
    }
}
