package scrapingtool.entities;

import java.util.Date;
import java.util.Optional;

/**
 * Created by cbougeno on 4/2/18.
 */
public class History {
    private Date time;
    private Optional<Float> energy = Optional.empty();
    private Optional<Float> efficiency = Optional.empty();
    private Optional<Integer> power = Optional.empty();
    private Optional<Integer> average = Optional.empty();
    private Optional<Float> normalised = Optional.empty();
    private Optional<Float> temperature = Optional.empty();
    private Optional<Float> voltage = Optional.empty();
    private Optional<Float> energyUsed = Optional.empty();
    private Optional<Integer> powerUsed = Optional.empty();

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Optional<Float> getEnergy() {
        return energy;
    }

    public void setEnergy(Optional<Float> energy) {
        this.energy = energy;
    }

    public Optional<Float> getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(Optional<Float> efficiency) {
        this.efficiency = efficiency;
    }

    public Optional<Integer> getPower() {
        return power;
    }

    public void setPower(Optional<Integer> power) {
        this.power = power;
    }

    public Optional<Integer> getAverage() {
        return average;
    }

    public void setAverage(Optional<Integer> average) {
        this.average = average;
    }

    public Optional<Float> getNormalised() {
        return normalised;
    }

    public void setNormalised(Optional<Float> normalised) {
        this.normalised = normalised;
    }

    public Optional<Float> getTemperature() {
        return temperature;
    }

    public void setTemperature(Optional<Float> temperature) {
        this.temperature = temperature;
    }

    public Optional<Float> getVoltage() {
        return voltage;
    }

    public void setVoltage(Optional<Float> voltage) {
        this.voltage = voltage;
    }

    public Optional<Float> getEnergyUsed() {
        return energyUsed;
    }

    public void setEnergyUsed(Optional<Float> energyUsed) {
        this.energyUsed = energyUsed;
    }

    public Optional<Integer> getPowerUsed() {
        return powerUsed;
    }

    public void setPowerUsed(Optional<Integer> powerUsed) {
        this.powerUsed = powerUsed;
    }

    @Override
    public String toString() {
        return "History{" +
                "time=" + time +
                ", energy=" + energy.orElse(null) +
                ", efficiency=" + efficiency.orElse(null) +
                ", power=" + power.orElse(null) +
                ", average=" + average.orElse(null) +
                ", normalised=" + normalised.orElse(null) +
                ", temperature=" + temperature.orElse(null) +
                ", voltage=" + voltage.orElse(null) +
                ", energyUsed=" + energyUsed.orElse(null) +
                ", powerUsed=" + powerUsed.orElse(null) +
                '}';
    }
}
