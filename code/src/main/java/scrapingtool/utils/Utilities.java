package scrapingtool.utils;

/**
 * Created by cbougeno on 17/2/18.
 */
public final class Utilities {

    public class Character {
        public static final String COMMA = ",";
        public static final String EMPTY = "";
        public static final String SPACE = "\\s";
    }

    public class Constant {
        public static final String URL_PVOUTPUT = "https://www.pvoutput.org/";
        public static final String URL_INDEX_PVOUTPUT = URL_PVOUTPUT + "index.jsp";
        public static final String URL_HTTP_HTTPS_PVOUTPUT_PATTERN = "https?\\:\\/\\/www\\.pvoutput\\.org\\/";
        public static final String USERNAME = "bousack@gmail.com";
        public static final String PASSWORD = ".skullPT0034.";
    }

    public class Dom {
        public static final String LOGIN = "login";
        public static final String PASSWORD = "password";
    }

    public class Pattern {
        public static final String DATE_FORMAT = "yyyyMMdd";
    }

    public class Regex {
        public static final String BEGIN_NUMBER_ALL_PATTERN = "^\\d.*";
        public static final String DATE_PATTERN = "dd/MM/yy-HH:mma";
        public static final String DATE_PATTERN_WITHOUT_A = "dd/MM/yy-HH:mm";
        public static final String STRING_PATTERN_DATE = "dd/MM/yyyy-HH:mm";
        public static final String DECIMAL_UNITS_PATTERN = "^(\\d+(?:(\\,)\\d+)?(\\.\\d+)?)(\\w*(?:\\/?\\w*)?)$";
        public static final String HEADER_PATTERN = "(.*)\\s+\\d+\\.?\\w+.*$";

        public static final String GROUP_1 = "$1";
        public static final String GROUP_2 = "$2";
    }
}
