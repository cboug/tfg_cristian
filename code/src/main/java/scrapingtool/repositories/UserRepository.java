package scrapingtool.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import scrapingtool.entities.User;

import java.util.List;

/**
 * Created by cbougeno on 10/1/18.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByName(String name);

    User findFirstByIdSid(String idSid);

}
