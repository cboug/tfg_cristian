# README #

Esta es una plantilla para LaTeX que puede ser utilizada para escribir los TFG del grado de IngenierÃ­a Multimedia de la Universidad de Alicante.

Para descargarla, ve a https://bitbucket.org/ppernias/tfg_im/downloads. Una vez descargado y descomprimido el fichero correspondiente se habrÃ¡ creado un directorio nuevo con varios ficheros y subdirectorios en Ã©l. CÃ¡mbiale el nombre al directorio principal por el que quieres utilizar. 

La plantilla estÃ¡ en revisiÃ³n, por lo que se recomienda utilizar siempre la Ãºltima versiÃ³n de la misma.

## DescripciÃ³n ##
La plantilla incluye, a modo de capÃ­tulo de introducciÃ³n y otro con el nombre de "marco teÃ³rico" ejemplos de escritura de diversas estructuras en LaTeX, como son:
 
* TÃ­tulos para CapÃ­tulos y Secciones
* Listas numeradas y no numeradas
* Tablas
* CÃ³mo insertar cÃ³digo fuente para su visualizaciÃ³n en LaTeX
* CÃ³mo insertar citas bibliogrÃ¡ficas.
* CÃ³mo insertar imÃ¡genes/ figuras y sus referencias

La plantilla genera automÃ¡ticamente:

* la portada a color (con los elementos identificativos del grado de IngenierÃ­a Multimedia de la UA. Si se va a imprimir esta portada aparte, eliminar la referencia correspondiente de la plantilla)
* Una segunda portada en blanco y negro con los datos de autor, y tutor(es)
* Ãndice general
* Ãndice de figuras, tablas y listados de cÃ³digo
* Estructura genÃ©rica para un TFG siguiendo las normas de la EPS de la UA.
* BibliografÃ­a en formato APA
* Anexos

La plantilla estÃ¡ pensada para una impresiÃ³n final del TFG a doble cara, con mÃ¡rgenes diferentes para pÃ¡ginas pares e impares.

AdemÃ¡s, genera lÃ­neas de cabecera para aquellas pÃ¡ginas que deben tenerla, asÃ­ como paginaciÃ³n en nÃºmeros romanos o arÃ¡bigos allÃ­ donde corresponde hacerlo de cada manera.

La plantilla posee muchas otras caracterÃ­sticas que pueden ser modificadas revisando el cÃ³digo LaTeX correspondiente.

## NOTAS importantes: ##

Hay dos portadas:

* Primera portada a color con los elementos identificativos del grado de IngenierÃ­a Multimedia de la Escuela PolitÃ©cnica de la UA. En realidad, esta portada es un PDF que se incrusta en la primera pagina del TFG sobre un fondo a color. Este PDF ha sido generado aparte, poniendo en Ã©l los elementos necesarios para una adecuada presentaciÃ³n. Se adjunta en este repositorio el fichero de esa portada en formato PSD para su ediciÃ³n con el software Photoshop o similar y posterior generaciÃ³n del PDF personalizado.

* Segunda portada en b/n. Esta portada recoge la informaciÃ³n suministrada en el fichero principal de la plantilla ("tfg_im.tex") en la secciÃ³n "InformaciÃ³n sobre el TFG". AllÃ­ se puede indicar el autor del TFG, el tÃ­tulo y subtÃ­tulo del TFG y el nombre y departamento del/los tutor/es. Comentar o borrar aquello que no se precise.

## GestiÃ³n de la bIbliografÃ­a ##

Se ha aÃ±adido un subdirectorio ("bibliografia") con un fichero de tipo **BIBTEX** con algunos ejemplos de entradas bibliogrÃ¡ficas. Este fichero estÃ¡ enlazado con la plantilla para su compilaciÃ³n conjunta. Se debe utilizar este fichero para la recopilaciÃ³n de la bibliografÃ­a por parte del alumno y su gestiÃ³n con el software que el alumno elija para ello. En el texto hay ejemplos de cÃ³mo se debe citar una fuente que exista en ese fichero.

## Software para la escritura del TFG en LaTeX ##

Existe mucho software para escribir el LaTeX dependiendo del S.O. que se estÃ© utiiizando. Una bÃºsqueda por Internet permitirÃ¡ encontrar el mÃ¡s adecuado a los gustos del autor. En http://tex.stackexchange.com/questions/339/latex-editors-ides hay una buena lista de editores LatTeX. Un ejemplo podrÃ­a ser:

**Para Windows**

  * TexWorks que se encuentra en http://www.tug.org/texworks/

**Para OSX**

  * La suite TexShop / Bibtex que puede ser descargada en http://pages.uoregon.edu/koch/texshop/
  * TexWorks: http://www.tug.org/texworks/

**Para Linux**

  * TexStudio http://texstudio.sourceforge.net/ (Recomendado por el prof. Juan RamÃ³n Rico)
  * TexMaker: http://alternativeto.net/software/texmaker/

Los editores LaTeX suelen venir acompaÃ±ados de un software para la gestiÃ³n de la bibliografÃ­a. Este software permite ir registrando las fuentes bibliogrÃ¡ficas en el formato de fichero BIBTEX (usar el mencionado anteriormente) y generar las referencias necesarias que hay que poner en LaTeX para que aparezcan correctamente en el texto y al final en el capÃ­tulo de "BibliografÃ­a" y que lo hagan en el formato correcto.

## AVISO ##
Esta plantilla no es perfecta. Puede ser mejorada con las aportaciones de muchos. Por favor, si puedes mejorarla, hazlo y comenta en los foros de este repositorio lo que has hecho. El resto de nosotros te estaremos muy agradecidos.

## Fuentes ##
La plantilla estÃ¡ basada en otras similares y en aportaciones de profesores de la UA:

* Plantilla para TFG de la U. de Granada: http://grados.ugr.es/informatica/pages/infoacademica/tfg/plantillas/plantilla_tfg_latex
* Aportaciones de Rafael Carrasco (DLSI Univ. de Alicante)

*
**Pedro PernÃ­as Peco. Coordinador AcadÃ©mico del grado de IngenierÃ­a Multimedia. Alicante, diciembre de 2014** 
*

--
**AmpliaciÃ³n**

Pedro Pastor (DSLSI) propone:

"... utilizar la clase {scrbook} en lugar de la clÃ¡sica {book} puede traer algunas complicaciones si no se domina el lenguaje. {scrbook} es mÃ¡s moderna (e incluye bastantes paquetes que en {book} hay que incluir aparte, pero si se quiere "customizar" algo hay que saber cÃ³mo, y no se hace igual que en {book} ( que es la que explican la mayorÃ­a de los manuales). Es decir, habrÃ­a que estudiarse el manual de KOMA-Script.
Por ejemplo, se que incluye algunos paquetes AMS para escribir matemÃ¡ticas, pero no se si falta alguno"

Por ahora, creo que es mejor dejar {scrbook} ya que cambiarlo por {book} genera una serie de errores en la compilaciÃ³n LaTeX que obligarÃ­an a cambiar muchas otras cosas y hasta que no las estudie, no se el efecto que tendrÃ­a. 

AdemÃ¡s, ha sugerido sustituir el paquete "color" por "xcolor" y reubicar la llamada al paquete "hyperref". Estas modificaciones se han incorporado  a la versiÃ³n actual de la plantilla.